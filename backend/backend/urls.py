from django.urls import path
from likely.schema import schema
from likely.drf.views import csrf, ReceiveNotificationsView
from graphene_file_upload.django import FileUploadGraphQLView

from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path("csrf/", csrf, name="csrf-getter"),
    path('notifications/', ReceiveNotificationsView.as_view(), name='notifications'),
    path("graphql/", FileUploadGraphQLView.as_view(schema=schema), name="graphql"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

