from rest_framework.serializers import ModelSerializer, SerializerMethodField
from likely.mdls.notification import Notification

class NotificationSerializer(ModelSerializer):
    from_user = SerializerMethodField()
    post = SerializerMethodField()

    class Meta:
        model = Notification
        fields = "__all__"

    def get_from_user(self, obj):
        user = obj.from_user
        user_data = {
            "id": user.id,
            "name": user.name or user.email,
        }
        return user_data

    def get_post(self, obj):
        post = obj.post
        if post:
            return {
                "id": post.id,
                "title": post.title
            }
        else:
            return None