import asyncio

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie

from likely.mdls.notification import Notification
from likely.drf.serializers import NotificationSerializer

from django.http import JsonResponse

class ReceiveNotificationsView(APIView):
    async def wait_for_new_notifications(
        self, user, notification, timeout, initial_length=None
    ):
        loop = asyncio.get_running_loop()

        async def check_notifications():
            if not initial_length and notification:
                new_notifications = await loop.run_in_executor(
                    None,
                    lambda: list(
                        user.notifications.filter(timestamp__gt=notification.timestamp)
                    ),
                )
                if new_notifications:
                    return new_notifications
            else:
                new_notifications = await loop.run_in_executor(
                    None,
                    lambda: list(user.notifications.filter().order_by("-timestamp")),
                )
                if len(new_notifications) > initial_length:
                    return new_notifications
            await asyncio.sleep(1)
            return await check_notifications()

        try:
            new_notifications = await asyncio.wait_for(check_notifications(), timeout)
        except asyncio.TimeoutError:
            return None

        return new_notifications
    
    def post(self, request):
        user = request.user
        if not user.is_authenticated:
            return Response(
                "User not authenticated", status=status.HTTP_401_UNAUTHORIZED
            )

        notification_id = request.data.get("notification_id")

        timeout = 55  # Timeout value in seconds

        if not notification_id:
            initial_length = user.notifications.count()

            try:
                new_notifications = asyncio.run(
                    self.wait_for_new_notifications(user, None, timeout, initial_length)
                )
            except RuntimeError:
                return Response(
                    "No new notifications", status=status.HTTP_204_NO_CONTENT
                )
        else:
            try:
                notification = Notification.objects.get(pk=notification_id)
            except Notification.DoesNotExist:
                return Response(
                    "Notification not found", status=status.HTTP_404_NOT_FOUND
                )

            new_notifications = list(
                user.notifications.filter(timestamp__gt=notification.timestamp)
            )
            if new_notifications:
                serializer = NotificationSerializer(new_notifications, many=True)
                return Response(serializer.data)

            try:
                new_notifications = asyncio.run(
                    self.wait_for_new_notifications(user, notification, timeout)
                )
            except RuntimeError:
                return Response(
                    "No new notifications", status=status.HTTP_204_NO_CONTENT
                )

        if new_notifications:
            serializer = NotificationSerializer(new_notifications, many=True)
            return Response(serializer.data)

        return Response("No new notifications", status=status.HTTP_204_NO_CONTENT)


@ensure_csrf_cookie
@csrf_exempt
def csrf(request):
    return JsonResponse({"csrfToken": "was sent"})