import graphene
import math
from graphql import GraphQLError
from django.contrib.sessions.models import Session

from likely.mdls.user import User
from likely.mdls.post import Post

from likely.graphene.types import UserType, PostType, PostsPaginationType
from likely.graphene.mutations.user import (
    LoginMutation,
    LogoutMutation,
    CreateUserMutation,
    UpdateUserMutation,
    DeleteUserMutation,
    DeleteNotificationMutation,
    SubscribeManageMutation,
)

from likely.graphene.mutations.post import (
    CreatePostMutation,
    DeletePostMutation,
    LikesManageMutation,
)

#####
## SCHEMA Mutations
####
class Mutation(graphene.ObjectType):
    ####
    # Auth mutations
    login = LoginMutation.Field()
    logout = LogoutMutation.Field()
    create_user = CreateUserMutation.Field()
    update_user = UpdateUserMutation.Field()
    delete_user = DeleteUserMutation.Field()
    delete_notification = DeleteNotificationMutation.Field()

    ####
    # Subscribe mutations
    subscribe_manage = SubscribeManageMutation.Field()

    ####
    # Post Mutations
    create_post = CreatePostMutation.Field()
    delete_post = DeletePostMutation.Field()
    likes_manage = LikesManageMutation.Field()


#####
## SCHEMA Queries
####
class Query(graphene.ObjectType):
    ####
    # Users Queries
    user = graphene.Field(UserType, id=graphene.ID(required=True))
    users = graphene.List(UserType, ids=graphene.List(graphene.ID, required=False))
    user_by_session = graphene.Field(UserType)

    def resolve_user(root, info, id):
        try:
            return User.objects.get(pk=id)
        except User.DoesNotExist:
            raise GraphQLError("User not found", extensions={"code": "NOT_FOUND"})

    def resolve_users(self, info, ids=None):
        if ids:
            return User.objects.filter(id__in=ids)[::-1]
        else:
            raise GraphQLError(
                "Ids not provided", extensions={"code": "INVALID_OPERATION"}
            )

    def resolve_user_by_session(self, info):
        session_id = info.context.session.session_key
        if session_id:
            try:
                session = Session.objects.get(session_key=session_id)
                user_id = session.get_decoded().get("_auth_user_id")
                user = User.objects.get(pk=user_id)
                if user.is_authenticated:
                    return user
            except Session.DoesNotExist:
                raise GraphQLError(
                    "Session is not valid", extensions={"code": "INVALID_OPERATION"}
                )
        else:
            raise GraphQLError("No session Provided", extensions={"code": "FORBIDDEN"})

    ####
    # Post Queries
    post = graphene.Field(PostType, id=graphene.ID(required=True))

    posts_paginated = graphene.Field(
        PostsPaginationType,
        page=graphene.Int(required=False),
        per_page=graphene.Int(required=False),
        ids=graphene.List(graphene.ID, required=False),
    )

    def resolve_posts_paginated(root, info, page=1, per_page=20, ids=None):
        total_pages = math.ceil(Post.objects.count() / per_page)
        if ids:
            queryset = Post.objects.all().filter(id__in=ids)[::-1]
            return PostsPaginationType(posts=queryset, total_pages=total_pages)
        else:
            queryset = Post.objects.all()[::-1]
            start_index = (page - 1) * per_page
            end_index = page * per_page
            paginated_queryset = queryset[start_index:end_index]
            return PostsPaginationType(posts=paginated_queryset, total_pages=total_pages)

    def resolve_post(root, info, id):
        return Post.objects.get(pk=id)



schema = graphene.Schema(query=Query, mutation=Mutation)
