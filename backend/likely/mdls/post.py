from django.db import models
from likely.mdls.user import User

from django.contrib.staticfiles.storage import staticfiles_storage


def upload_image_to(instance, filename):
    return f"images/{instance.author.id}/{filename}"


class Post(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    image = models.ImageField(upload_to=upload_image_to, blank=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="posts")
    likes = models.ManyToManyField(User, related_name="liked", blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
