from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

def upload_image_to(instance, filename):
    return f"images/{instance.id}/{filename}"

class User(AbstractUser):
    name = models.CharField(max_length=255)
    avatar = models.ImageField(upload_to=upload_image_to, blank=True)
    email = models.EmailField(unique=True)
    subscribers = models.ManyToManyField("self", symmetrical=False, related_name="subscriptions")
    notifications = models.ManyToManyField("Notification", related_name="to_users")

    def delete(self, *args, **kwargs):
        self.posts.all().delete()
        super().delete(*args, **kwargs)