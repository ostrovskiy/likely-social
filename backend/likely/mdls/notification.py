from django.db import models
from likely.mdls.user import User
from likely.mdls.post import Post


class Notification(models.Model):
    ACTION_CHOICES = (
        ("subscribed", "Subscribed"),
        ("unsubscribed", "Unsubscribed"),
        ("like", "like"),
        ("unlike", "unlike"),
        ("post", "Post"),
        ("removePost", "RemovePost")
    )
    id = models.AutoField(primary_key=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, blank=True, null=True)
    from_user = models.ForeignKey(User, on_delete=models.CASCADE)
    action = models.CharField(max_length=64, choices=ACTION_CHOICES)
    timestamp = models.DateTimeField(auto_now_add=True)

