from django.apps import AppConfig


class LikelyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'likely'