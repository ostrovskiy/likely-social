from likely.mdls.notification import Notification
from likely.mdls.user import User


def notify(user, action, target_user=None, post=None):
    notification = Notification.objects.create(from_user=user, action=action, post=post)
    to_users = []

    if target_user:
        to_users.append(target_user)
    else:
        to_users = list(user.subscribers.all())

    notification.to_users.set(to_users)


