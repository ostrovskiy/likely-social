from django.contrib.auth import get_user_model

from likely.helpers.notifyer import notify


@staticmethod
def subscription_manager(user, target_user):
    if target_user.subscribers.filter(pk=user.pk).exists():
        target_user.subscribers.remove(user)
        notify(user=user, action='unsubscribed', target_user=target_user)
        return 'unsubscribed'
    else:
        target_user.subscribers.add(user)
        notify(user=user, action='subscribed', target_user=target_user)
        return 'subscribed'
