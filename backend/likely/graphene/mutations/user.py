import graphene
from graphql import GraphQLError
from graphene_file_upload.scalars import Upload

from django.contrib.auth import get_user_model, authenticate, login, logout

from likely.graphene.inputs import UserInput
from likely.graphene.types import UserType

from likely.helpers.subscription_manager import subscription_manager


class LoginMutation(graphene.Mutation):
    class Arguments:
        data = UserInput(required=True)

    user = graphene.Field(UserType)

    @staticmethod
    def mutate(root, info, data):
        user = info.context.user

        if user.is_authenticated:
            raise GraphQLError(
                "You are already logged in.", extensions={"code": "ALREADY_LOGGED_IN"}
            )

        user = authenticate(
            info.context, username=data["email"], password=data["password"]
        )
        if user:
            login(info.context, user)
            return LoginMutation(user=user)

        raise GraphQLError(
            "Invalid email or password.", extensions={"code": "AUTHENTICATION_FAILED"}
        )


class LogoutMutation(graphene.Mutation):
    status = graphene.String()

    @staticmethod
    def mutate(root, info):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You are not logged in.", extensions={"code": "AUTHENTICATION_REQUIRED"}
            )

        logout(info.context)
        return LogoutMutation(status="LOGOUT")


class CreateUserMutation(graphene.Mutation):
    class Arguments:
        data = UserInput(required=True)

    user = graphene.Field(UserType)

    @staticmethod
    def mutate(root, info, data):
        user = get_user_model()(**data, username=data["email"])
        user.set_password(data["password"])
        user.save()
        return CreateUserMutation(user=user)


class UpdateUserMutation(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        name = graphene.String(required=False)
        avatar = Upload(required=False)
        password = graphene.String(required=False)
        email = graphene.String(required=False)

    @staticmethod
    def mutate(root, info, name=None, avatar=None, email=None, password=None):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You are not logged in.", extensions={"code": "AUTHENTICATION_REQUIRED"}
            )

        if name or email or avatar or password:
            if name:
                user.name = name
            if avatar:
                user.avatar.save(avatar.name, avatar)
            if email:
                user.email = email
                user.username = email
            if password:
                user.set_password(password)
            user.save()

        return UpdateUserMutation(user=user)


class DeleteUserMutation(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        id = graphene.ID(required=True)

    @staticmethod
    def mutate(root, info, id):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You are not logged in.", extensions={"code": "AUTHENTICATION_REQUIRED"}
            )

        if str(user.id) != id:
            raise GraphQLError(
                "You are not authorized to delete this user.",
                extensions={"code": "FORBIDDEN"},
            )

        try:
            user.delete()
        except Exception as e:
            raise GraphQLError(str(e), extensions={"code": "INTERNAL_SERVER_ERROR"})

        return DeleteUserMutation(user=None)


class SubscribeManageMutation(graphene.Mutation):
    target_user = graphene.Field(UserType)
    current_user = graphene.Field(UserType)

    class Arguments:
        user_id = graphene.ID(required=True)

    status = graphene.String()

    @staticmethod
    def mutate(root, info, user_id):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You must be logged in to perform this action.",
                extensions={"code": "AUTHENTICATION_REQUIRED"},
            )

        if str(user.id) == user_id:
            raise GraphQLError(
                "You can't subscribe to yourself.",
                extensions={"code": "INVALID_OPERATION"},
            )

        try:
            target_user = get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            raise GraphQLError("User not found.", extensions={"code": "NOT_FOUND"})

        status = subscription_manager(user, target_user)

        return SubscribeManageMutation(
            target_user=target_user, current_user=user, status=status
        )


class DeleteNotificationMutation(graphene.Mutation):
    status = graphene.String()

    class Arguments:
        notification_id = graphene.ID(required=True)

    @staticmethod
    def mutate(root, info, notification_id):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You are not logged in.", extensions={"code": "AUTHENTICATION_REQUIRED"}
            )

        try:
            notification = user.notifications.get(id=notification_id)
            user.notifications.remove(notification)
            status = "deleted"
        except user.notifications.model.DoesNotExist:
            raise GraphQLError(
                "Notification was not found", extensions={"code": "NOT_FOUND"}
            )
        except Exception as e:
            raise GraphQLError(str(e), extensions={"code": "INTERNAL_SERVER_ERROR"})

        return DeleteNotificationMutation(status=status)
