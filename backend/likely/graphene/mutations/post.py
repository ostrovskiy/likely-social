import graphene
from graphql import GraphQLError
from graphene_file_upload.scalars import Upload

from likely.mdls.post import Post
from likely.graphene.types import PostType

from likely.helpers.notifyer import notify



class CreatePostMutation(graphene.Mutation):
    post = graphene.Field(PostType)

    class Arguments:
        title = graphene.String(required=True)
        content = graphene.String(required=False)
        image = Upload(required=False)

    @staticmethod
    def mutate(root, info, title, content, image=None):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You must be logged in to create a post.",
                extensions={"code": "AUTHENTICATION_REQUIRED"}
            )

        post = Post(title=title, content=content, author=user)

        if image:
            post.image.save(image.name, image)

        post.save()
        notify(user=user, action="post", post=post)

        return CreatePostMutation(post=post)


class DeletePostMutation(graphene.Mutation):
    status = graphene.String()

    class Arguments:
        id = graphene.ID(required=True)

    @staticmethod
    def mutate(root, info, id):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You must be logged in to delete a post.",
                 extensions={"code": "AUTHENTICATION_REQUIRED"}
            )

        try:
            post = Post.objects.get(pk=id)
            if post.author == user:
                post.delete()
                return DeletePostMutation(status="Deleted")
            else:
                raise GraphQLError(
                    "You can only delete your own posts.",  extensions={"code": "FORBIDDEN"}
                )
        except Post.DoesNotExist:
            raise GraphQLError("Post not found.", extensions={"code": "NOT_FOUND"})


class LikesManageMutation(graphene.Mutation):
    post = graphene.Field(PostType)
    status = graphene.String()

    class Arguments:
        id = graphene.ID(required=True)

    @staticmethod
    def mutate(root, info, id):
        user = info.context.user

        if not user.is_authenticated:
            raise GraphQLError(
                "You must be logged in to manage likes.", extensions={"code": "AUTHENTICATION_REQUIRED"}
            )

        post = Post.objects.get(pk=id)

        if post.likes.filter(id=user.id).exists():
            post.likes.remove(user)
            status = "unlike"
        else:
            post.likes.add(user)
            status = "like"

        if user.id != post.author.id:
            notify(user=user, action=status, target_user=post.author, post=post)

        return LikesManageMutation(post=post, status=status)
