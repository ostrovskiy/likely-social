import graphene
from graphene_django import DjangoObjectType

from likely.mdls.user import User
from likely.mdls.notification import Notification
from likely.mdls.post import Post

class UserType(DjangoObjectType):
    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "avatar",
            "email",
            "subscribers",
            "subscribers_count",
            "is_subscriber",
            "subscriptions",
            "subscriptions_count",
            "is_subscripter",
            "notifications",
            "posts",
            "liked",
        )

    def resolve_subscribers(self, info):
        return self.subscribers.all()[::-1][:10]

    subscribers_count = graphene.Int()

    def resolve_subscribers_count(self, info):
        return self.subscribers.count()

    is_subscriber = graphene.Boolean()

    def resolve_is_subscriber(self, info):
        user = info.context.user
        if user.is_authenticated:
            return self.subscribers.filter(id=user.id).exists()
        return False

    def resolve_subscriptions(self, info):
        return self.subscriptions.all()[::-1][:10]

    subscriptions_count = graphene.Int()

    def resolve_subscriptions_count(self, info):
        return self.subscriptions.count()

    is_subscripter = graphene.Boolean()

    def resolve_is_subscripter(self, info):
        user = info.context.user
        if user.is_authenticated:
            return self.subscriptions.filter(id=user.id).exists()
        return False

    def resolve_liked(self, info):
        return self.liked.all()[::-1][:10]

    def resolve_notifications(self, info):
        return self.notifications.all()[::-1][:10]

    def resolve_posts(self, info):
        return self.posts.all()[::-1][:10]


class NotificationType(DjangoObjectType):
    class Meta:
        model = Notification
        fields = ("id", "from_user", "action", "timestamp", "post")


class PostType(DjangoObjectType):
    class Meta:
        model = Post
        fields = (
            "id",
            "title",
            "image",
            "content",
            "timestamp",
            "author",
            "likes",
            "is_liked",
            "likes_count",
        )

    def resolve_likes(self, info):
        return self.likes.all()[::-1]

    is_liked = graphene.Boolean()

    def resolve_is_liked(self, info):
        user = info.context.user
        if user.is_authenticated:
            return self.likes.filter(id=user.id).exists()
        return False

    likes_count = graphene.Int()

    def resolve_likes_count(self, info):
        return self.likes.count()
    
class PostsPaginationType(graphene.ObjectType):
    posts = graphene.List(PostType)
    total_pages = graphene.Int()