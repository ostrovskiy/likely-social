import type { ApolloClientOptions } from '@apollo/client/core'
import { InMemoryCache } from '@apollo/client/core'
import { createUploadLink } from 'apollo-upload-client'
import { setContext } from '@apollo/client/link/context'
import { schema } from 'src/apollo/shema'
import { authHelper } from 'src/helpers/auth'
import { Cookies } from 'quasar'

////////
// Setting Up Apollo
const authLink = setContext((_, { headers }) => {
  authHelper.csrfToken.value = Cookies.get('csrftoken') || ''
  return {
    headers: {
      ...headers,
      'X-Csrftoken': authHelper.csrfToken.value,
    },
  }
})

const cache = new InMemoryCache({
  typePolicies: {
    UserType: {
      fields: {
        subscribers: {
          merge: false,
        },
        subscriptions: {
          merge: false,
        },
      },
    },
  },
})

export function getClientOptions() {
  // /* {app, router, ...} */ options?: Partial<BootFileParams<any>>
  return <ApolloClientOptions<unknown>>Object.assign(
    // General options.
    <ApolloClientOptions<unknown>>{
      link: authLink.concat(
        createUploadLink({
          uri: `${process.env.API_URI}/graphql/`,
          credentials: 'include',
        })
      ),
      typeDefs: schema,
      cache: cache,
    },

    // Specific Quasar mode options.
    process.env.MODE === 'spa'
      ? {
          //
        }
      : {},
    process.env.MODE === 'ssr'
      ? {
          //
        }
      : {},
    process.env.MODE === 'pwa'
      ? {
          //
        }
      : {},
    process.env.MODE === 'bex'
      ? {
          //
        }
      : {},
    process.env.MODE === 'cordova'
      ? {
          //
        }
      : {},
    process.env.MODE === 'capacitor'
      ? {
          //
        }
      : {},
    process.env.MODE === 'electron'
      ? {
          //
        }
      : {},

    // dev/prod options.
    process.env.DEV
      ? {
          //
        }
      : {},
    process.env.PROD
      ? {
          //
        }
      : {},

    // For ssr mode, when on server.
    process.env.MODE === 'ssr' && process.env.SERVER
      ? {
          ssrMode: true,
        }
      : {},
    // For ssr mode, when on client.
    process.env.MODE === 'ssr' && process.env.CLIENT
      ? {
          ssrForceFetchDelay: 100,
        }
      : {}
  )
}
