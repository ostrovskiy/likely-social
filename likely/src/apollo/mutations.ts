import gql from 'graphql-tag'

////////
// User Mutations
//////
export const LOGIN = gql`
  mutation login($credentials: UserInput!) {
    login(data: $credentials) {
      user {
        id
        name
        email
        avatar
        posts {
          id
          title
          image
          timestamp
        }
        subscribers {
          id
          name
          email
          avatar
          isSubscripter
        }
        subscribersCount
        subscriptions {
          id
          name
          email
          avatar
          isSubscriber
        }
        subscriptionsCount
        liked {
          id
          title
          image
          isLiked
        }
        notifications {
          id
          fromUser {
            name
            email
            id
          }
          timestamp
          action
        }
      }
    }
  }
`

export const CREATE_USER = gql`
  mutation createUser($credentials: UserInput!) {
    createUser(data: $credentials) {
      user {
        id
        email
      }
    }
  }
`

export const LOGOUT = gql`
  mutation logout {
    logout {
      status
    }
  }
`

export const UPDATE_USER = gql`
  mutation updateUser(
    $name: String
    $email: String
    $password: String
    $avatar: Upload
  ) {
    updateUser(
      name: $name
      email: $email
      password: $password
      avatar: $avatar
    ) {
      user {
        id
        name
        avatar
        email
      }
    }
  }
`

export const SUBSCRIBE_MANAGE = gql`
  mutation subscribeManage($id: ID!) {
    subscribeManage(userId: $id) {
      targetUser {
        id
        isSubscriber
        subscribersCount
        subscribers {
          id
          name
          avatar
          email
        }
      }
      currentUser {
        id
        isSubscripter
        subscriptionsCount
        subscriptions {
          id
          name
          avatar
          email
        }
      }
      status
    }
  }
`

////////
// Post Mutations
//////
export const LIKES_MANAGE = gql`
  mutation likesManage($id: ID!) {
    likesManage(id: $id) {
      post {
        id
        likesCount
        isLiked
      }
      status
    }
  }
`

export const CREATE_POST = gql`
  mutation createPost($title: String!, $content: String, $image: Upload) {
    createPost(title: $title, content: $content, image: $image) {
      post {
        id
        title
        content
        timestamp
        image
        likesCount
        isLiked
        author {
          id
          name
          email
          avatar
        }
      }
    }
  }
`
