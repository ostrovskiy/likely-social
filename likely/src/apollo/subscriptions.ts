import gql from 'graphql-tag'

export const NEW_POST_SUBSCRIPTION = gql`
  subscription newPost {
    newPost {
      id
      title
      content
      timestamp
      likesCount
      isLiked
      author {
        id
        name
        email
        avatar
      }
    }
  }
`
