import gql from 'graphql-tag'

export const schema = gql`
  type User {
    id: ID!
    name: String
    subscribers: [User]
    subscriptions: [User]
    subscribersCount: String!
    subscriptionsCount: String!
    liked: [Post!]!
  }

  type Post {
    id: ID!
    title: String
    content: String
    image: String
    author: User!
    timestamp: DateTime
    likes: [Post]
    likesCount: String!
    isLiked: Boolean
  }

  type Notification {
    id: ID!
    action: String!
    from_user: User!
    to_users: [User]
    timestamp: DateTime!
  }

  type UserNotificationsType {
    id: ID!
    user: User!
    notifications: [Notification]
  }
`
