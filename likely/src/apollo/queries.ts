import gql from 'graphql-tag'

////////
// Posts Queries
///////
export const GET_POSTS = gql`
  query GetPosts($ids: [ID], $page: Int, $perPage: Int) {
    postsPaginated(ids: $ids, page: $page, perPage: $perPage) {
      posts {
        id
        title
        content
        image
        timestamp
        likesCount
        isLiked
        author {
          id
          name
          email
          avatar
        }
      }
      totalPages
    }
  }
`

export const GET_POST_BY_ID = gql`
  query GetPostById($id: ID!) {
    post(id: $id) {
      id
      title
      image
      content
      timestamp
      author {
        id
        name
        email
        avatar
      }
      likes {
        id
        email
      }
      isLiked
      likesCount
    }
  }
`

////////
// User Queries
///////
export const GET_USERS_BY_IDS = gql`
  query GetUsers($ids: [ID]!) {
    users(ids: $ids) {
      id
      name
      avatar
      email
      isSubscriber
      isSubscripter
      subscribers {
        id
        avatar
        name
        email
      }
      subscriptions {
        id
        avatar
        name
        email
      }
      posts {
        title
        timestamp
      }
      liked {
        title
        id
      }
    }
  }
`

export const GET_USER_BY_ID = gql`
  query GetUserById($id: ID!) {
    user(id: $id) {
      id
      name
      avatar
      email
      isSubscriber
      subscribersCount
      isSubscripter
      subscriptionsCount
      subscribers {
        id
        name
        email
        avatar
      }
      subscriptions {
        id
        name
        email
        avatar
      }
      liked {
        id
        title
        image
        timestamp
      }
      posts {
        id
        title
        image
        timestamp
      }
    }
  }
`

export const GET_USER_BY_SESSION = gql`
  query userBySession {
    userBySession {
      id
      name
      avatar
      email
      notifications {
        id
        action
        timestamp
        fromUser {
          id
          name
          email
        }
      }
      subscribers {
        id
        name
        email
        avatar
      }
      subscribersCount
      subscriptions {
        id
        name
        email
        avatar
      }
      subscriptionsCount
      liked {
        id
        title
        image
        timestamp
      }
      posts {
        id
        title
        image
        timestamp
      }
    }
  }
`

export const GET_AUTH_USER_NOTIFICAIONS = gql`
  query userAuthNotifications {
    userBySession {
      id
      subscribers {
        id
        name
        email
        avatar
        subscriptionsCount
        subscriptions {
          id
          name
          avatar
          email
        }
      }
      subscribersCount
      subscriptions {
        id
        name
        email
        avatar
        subscriptionsCount
        subscribers {
          id
          name
          avatar
          email
        }
      }
      subscriptionsCount
      notifications {
        id
        action
        timestamp
        fromUser {
          id
          name
          email
        }
      }
    }
  }
`

export const GET_AUTH_USER_UPDATES = gql`
  query userAuthUpdates {
    userBySession {
      id
      liked {
        id
        title
        image
        timestamp
      }
      posts {
        id
        title
        image
        timestamp
      }
    }
  }
`
