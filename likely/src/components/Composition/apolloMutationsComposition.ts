import { apolloClient } from 'src/boot/apollo'
import { useMutation, provideApolloClient } from '@vue/apollo-composable'

import {
  fetchMorePosts,
  refetchAuthUser,
} from 'src/components/Composition/apolloQueriesComposition'

// Import Mutations
import {
  CREATE_USER,
  LOGIN,
  LIKES_MANAGE,
  LOGOUT,
  CREATE_POST,
  SUBSCRIBE_MANAGE,
  UPDATE_USER,
} from 'src/apollo/mutations'

//////
// Provide Apollo
provideApolloClient(apolloClient)

//////
// Apollo Mutations
//////

//////
// User
// signUp
export const { mutate: signUpMutation } = useMutation(CREATE_USER)

// signIn
export const { mutate: signInMutation } = useMutation(LOGIN)

// subscribeManage
export const { mutate: subscribeManageMutation } = useMutation(SUBSCRIBE_MANAGE)

// updateUserInfo
export const { mutate: updateUserMutation } = useMutation(UPDATE_USER)

// signOut
export const { mutate: signOutMutation } = useMutation(LOGOUT)

//////
// Posts
// Set/Remove Like
export const { mutate: likesManageMutation } = useMutation(LIKES_MANAGE)

// Create Post
export const { mutate: createPostMutation } = useMutation(CREATE_POST, {
  update: async (cache, { data: { createPost } }) => {
    await fetchMorePosts([parseInt(createPost.post.id)])
    await refetchAuthUser()
  },
})
