import { apolloClient } from 'src/boot/apollo'
import { useQuery, provideApolloClient } from '@vue/apollo-composable'

import {
  GET_POSTS,
  GET_POST_BY_ID,
  GET_AUTH_USER_UPDATES,
  GET_AUTH_USER_NOTIFICAIONS,
  GET_USER_BY_ID,
} from 'src/apollo/queries'

import { ref, computed } from 'vue'
import { Post } from 'src/interfaces/entitys'

//////
// Provide Apollo
provideApolloClient(apolloClient)

//////
// Variables
export const page = ref(1)
export const perPage = ref(10)

//////
// Computed
export const totalPages = computed(() => {
  return resultPosts?.value?.postsPaginated.totalPages ?? 1
})

//////
// Posts
export const {
  loading: loadingPosts,
  result: resultPosts,
  fetchMore: fetchMorePostsQuery,
  refetch: refetchPostsQuery,
} = useQuery(GET_POSTS, {
  page: page.value,
  perPage: perPage.value,
  ids: null as number[] | null,
})

export async function fetchMorePosts(ids?: number[]): Promise<void> {
  await fetchMorePostsQuery({
    variables: {
      page: ids ? 1 : ++page.value,
      ids: ids,
    },
    updateQuery: (previousResult, { fetchMoreResult }) => {
      if (!fetchMoreResult) return previousResult
      if (!(ids && ids?.length)) {
        return {
          postsPaginated: {
            ...fetchMoreResult.postsPaginated,
            posts: [
              ...previousResult.postsPaginated.posts,
              ...fetchMoreResult.postsPaginated.posts,
            ],
          },
        }
      } else {
        return {
          postsPaginated: {
            ...fetchMoreResult.postsPaginated,
            posts: resolvePosts(
              previousResult.postsPaginated.posts,
              fetchMoreResult.postsPaginated.posts
            ),
          },
        }
      }
    },
  })
}

// Post By ID
export const getPostById = (id: string) => {
  const { result, loading, refetch } = useQuery(GET_POST_BY_ID, () => ({
    id: id,
  }))
  return { result, loading, refetch }
}

//////
// Users

// Get User Updates as Subscribers and subscriptions
export const { result: resultAuthUser, refetch: refetchAuthUser } = useQuery(
  GET_AUTH_USER_UPDATES,
  null,
  {
    fetchPolicy: 'network-only',
  }
)

// Get Only Notificaitons
export const {
  result: resultNotificaionsUser,
  refetch: refetchNotificaionsUser,
} = useQuery(GET_AUTH_USER_NOTIFICAIONS, null, {
  fetchPolicy: 'network-only',
})

// User By ID
export const getUserById = (id: string) => {
  const { result, loading, refetch } = useQuery(GET_USER_BY_ID, () => ({
    id: id,
  }))
  return { result, loading, refetch }
}

//////
// Helpers
function resolvePosts(oldArray: Post[], newArray: Post[]) {
  const mergedArray = [...oldArray]

  newArray.forEach((newObj) => {
    const index = mergedArray.findIndex((oldObj) => oldObj.id === newObj.id)
    if (index !== -1) {
      mergedArray[index] = newObj
    } else {
      mergedArray.push(newObj)
    }
  })

  return mergedArray
}
