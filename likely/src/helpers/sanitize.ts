import sanitizeHtml from 'sanitize-html'

interface SanitizeOptions {
  [key: string]: {
    allowedTags: string[]
    allowedAttributes: {
      [key: string]: string[]
    }
    allowedIframeHostnames: string[]
  }
}

export function cleanContent(dirty: string, type = 'secured'): string {
  const options: SanitizeOptions = {
    secured: {
      allowedTags: ['b', 'i', 'em', 'strong', 'strike', 'div', 'a', 'br'],
      allowedAttributes: {
        a: ['href'],
      },
      allowedIframeHostnames: ['www.youtube.com'],
    },
    full: {
      allowedTags: [],
      allowedAttributes: {
        a: [],
      },
      allowedIframeHostnames: [],
    },
  }
  return sanitizeHtml(dirty, options[type])
}
