// Helpers
function formatTime(timestamp: Date) {
  const options: Intl.DateTimeFormatOptions = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
  }
  return timestamp
    ? new Date(timestamp).toLocaleDateString(navigator.language, options)
    : ''
}

export { formatTime }
