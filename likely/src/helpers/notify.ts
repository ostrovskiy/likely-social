import { Notify, QNotifyOptions, QNotifyAction } from 'quasar'

export function createNotify(
  message: string,
  color = 'red',
  html?: boolean,
  actions?: QNotifyAction[],
  icon = 'eva-alert-triangle-outline',
  timeout = 5000
): void {
  const options: QNotifyOptions = {
    progress: true,
    html: html,
    message: message,
    type: 'positive',
    color: color,
    icon: icon,
    position: 'top-right',
    timeout: timeout,
    actions: actions,
  }

  Notify.create(options)
}
