import { NotificationMessage } from 'src/interfaces/entitys'
import { ref, Ref } from 'vue'
import { createNotify } from 'src/helpers/notify'
import { routerHelper } from 'src/boot/routerHelper'
import { authHelper } from 'src/helpers/auth'
import {
  refetchNotificaionsUser,
  fetchMorePosts,
  refetchAuthUser,
} from 'src/components/Composition/apolloQueriesComposition'

export const notificationId = ref<number>()

// Usage
export const notificationsList: Ref<NotificationMessage[]> = ref([])

export async function notificaionReciever(): Promise<void> {
  const headers = new Headers()
  headers.set('Content-Type', 'application/json')
  headers.set('X-Csrftoken', authHelper.csrfToken.value)

  try {
    const response = await fetch(`${process.env.API_URI}/notifications/`, {
      method: 'post',
      credentials: 'include',
      headers: headers,
      body: JSON.stringify({ notification_id: notificationId.value }),
    })

    if (!response.ok) {
      throw new Error('Server returned an error.')
    }

    const data = await response.json()
    notificationId.value = data[0].id
    handleNotificaions(data)
    refetchNotificaionsUser()
    await notificaionReciever()
  } catch (error) {
    await new Promise((resolve) => setTimeout(resolve, 5000))
    await notificaionReciever()
  }
}

function handleNotificaions(data: NotificationMessage[]): void {
  const setPosts: Set<number> = new Set()
  data.forEach((n) => {
    if (n.post?.id) {
      setPosts.add(n.post?.id)
    }
    const template = !n.post?.id
      ? `User <span class="n-link">${n.from_user.name}</span> ${n.action}`
      : `User <span class="n-link" >${n.from_user.name}</span> ${n.action} <span class="n-link">${n.post?.title}</span>`
    const actions = [
      {
        label: 'Go to User',
        color: 'white',
        handler: () => {
          routeTo(`/users/${n.from_user.id}`)
        },
      },
      {
        label: 'Go to Post',
        color: 'white',
        handler: () => {
          routeTo(`/posts/${n.post?.id}`)
        },
      },
    ]
    createNotify(template, 'blue-10', true, n.post?.id ? actions : [actions[0]])
  })
  if (setPosts.size) {
    fetchMorePosts(Array.from(setPosts))
  }
  refetchAuthUser()
}

function routeTo(link: string): void {
  routerHelper?.push(link)
}
