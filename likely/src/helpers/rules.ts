// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

//////
// required rule
export const required = (v) => v.length >= 6 || 'Minimum 6 characters reqired'

//////
// email rule
export const email = (v) =>
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    v
  ) || 'Email not valid'

//////
// Password confirmtion
export const comparePasswords = (v, passwordValue) => {
  return v === passwordValue || 'Passwords mismatch'
}

export const emailOrEmpty = (v) => !v || email(v) || 'Email not valid'
