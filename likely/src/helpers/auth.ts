import { ref } from 'vue'

import { useQuery } from '@vue/apollo-composable'
import { GET_USER_BY_SESSION } from 'src/apollo/queries'

import { User } from 'src/interfaces/entitys'
import { Cookies } from 'quasar'

export const authHelper = {
  authUser: ref<User>(),
  isAuthenticated: ref<boolean>(false),
  csrfToken: ref<string>(''),

  setUser(user: User): void {
    this.authUser.value = user
    this.isAuthenticated.value = true
  },

  updateCSRF() {
    this.csrfToken.value = Cookies.get('csrftoken') || ''
  },

  removeUser(): void {
    this.authUser.value = undefined
    this.isAuthenticated.value = false
  },

  tryGetUser(): void {
    const { onResult, onError } = useQuery(GET_USER_BY_SESSION, {
      fetchPolicy: 'no-cache',
    })
    onResult((result) => {
      if (result?.data) {
        authHelper.setUser(result?.data?.userBySession)
        this.isAuthenticated.value = true
      }
    })
    onError(() => {
      this.isAuthenticated.value = false
    })
  },
}
