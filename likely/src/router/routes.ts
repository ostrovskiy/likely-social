import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      {
        path: '/posts/:id',
        component: () => import('pages/posts/PostPage.vue'),
        name: 'PostPage',
      },
      {
        path: '/users/:id',
        component: () => import('pages/users/UserPage.vue'),
        name: 'UserPage',
      },
    ],
  },

  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
]

export default routes
