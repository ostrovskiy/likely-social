export interface loginForm {
  email: string
  password: string
}

export interface updateUserInfo {
  email?: string
  password?: string
  name?: string
  avatar?: File
}

export interface postForm {
  title: string
  image?: File
  content: string
}

// export interface validationLoginError {

// }
