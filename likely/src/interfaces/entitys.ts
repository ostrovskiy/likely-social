export interface NotificationMessage {
  id: number
  post?: Post
  from_user: User
  action: string
  timestamp: Date
}

export interface User {
  id: number
  name: string
  avatar: string
  email: string
  subscribers: Array<User>
  subscribersCount: number
  subscriptions: Array<User>
  subscriptionsCount: number
  notifications: Array<NotificationMessage>
  isSubscriber: boolean
  isSubscripter: boolean
  liked: Array<Post>
  posts: Array<Post>
}

export interface authUserModel {
  user: User
}

export interface Post {
  id: number
  title: string
  content: string
  image: string
  author: User
  likes: Array<User>
  likesCount: number
  isLiked: boolean
  timestamp: Date
}

export interface PostsPaginated {
  posts: Post[]
  totalPages: number
}
