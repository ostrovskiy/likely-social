import { boot } from 'quasar/wrappers'
import { Router } from 'vue-router'

export let routerHelper: Router | null = null

export default boot(async ({ router }) => {
  routerHelper = router
})
