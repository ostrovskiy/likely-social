import { ApolloClient /*, createHttpLink */ } from '@apollo/client/core'
import { ApolloClients } from '@vue/apollo-composable'
import { boot } from 'quasar/wrappers'
import { getClientOptions } from 'src/apollo'

////////
// FETCH CSRF TOKEN VIA RESTAPI
async function prepeareConnection() {
  try {
    await fetch(`${process.env.API_URI}/csrf/`, {
      method: 'get',
      credentials: 'include',
    })
  } catch (error) {
    console.log(error)
  }
}

const options = getClientOptions()
export const apolloClient = new ApolloClient(options)

export default boot(async ({ app }) => {
  await prepeareConnection()
  const apolloClients: Record<string, ApolloClient<unknown>> = {
    default: apolloClient,
  }

  app.provide(ApolloClients, apolloClients)
})
